# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/tmm.png" height="48" width="48" /> ![tinyMediaManager](https://img.shields.io/chocolatey/v/tinymediamanager.svg?label=tinyMediaManager&style=for-the-badge)](https://chocolatey.org/packages/tinymediamanager)

[tinyMediaManager](http://www.tinymediamanager.org) is a media management tool written in Java/Swing. It is written to provide metadata for the XBOX Media Center (XBMC). Due to the fact that it is written in Java, tinyMediaManager will run on Windows, Linux and Mac OSX (and possible more OS).

## Features

- [All Features](http://www.tinymediamanager.org/features)

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/tinymediamanager.png)
