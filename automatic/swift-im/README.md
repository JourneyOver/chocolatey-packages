# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/swift-im.png" height="48" width="48" /> ![Swift](https://img.shields.io/chocolatey/v/swift-im.svg?label=Swift.IM&style=for-the-badge)](https://chocolatey.org/packages/swift-im)

Swift.im is an elegant, secure, adaptable and intuitive XMPP client, with features that make it suitable for a wide range of use scenarios.

## Features

- sending and receiving plain text messages
- emoticons
- basic statuses
- avatar support
- mailbox status support

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/swift-im.png)
