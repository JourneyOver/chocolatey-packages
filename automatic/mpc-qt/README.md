# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/mpc-qt.png" height="48" width="48" /> ![MPC QT](https://img.shields.io/chocolatey/v/mpc-qt.svg?label=MPC%20QT&style=for-the-badge)](https://chocolatey.org/packages/mpc-qt)

Media Player Classic Qute Theater (mpc-qt) is a clone of Media Player Classic (mpc-hc) reimplemented in Qt. It aims to reproduce most of the interface and functionality of mpc-hc while using libmpv to play video instead of DirectShow.

## Features

- Multiple playlists
- Quick queuing
- Playlist searching
- Screenshot templates
- Looped playback
- Custom metadata

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/mpc-qt.png)
