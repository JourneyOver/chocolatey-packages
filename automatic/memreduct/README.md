# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@25bdfa16dc70cbc99cdfee77eaff56714fc57a14/icons/memreduct.png" height="48" width="48" /> ![Mem Reduct](https://img.shields.io/chocolatey/v/memreduct.svg?label=Mem%20Reduct&style=for-the-badge)](https://chocolatey.org/packages/memreduct)

Lightweight real-time memory management application to monitor and clean system memory on your computer.

The program used undocumented internal system features (Native API) to clear system cache (system working set, working set, standby page lists, modified page lists) with variable result ~10-50%. Application it is compatible with Windows XP SP3 and higher operating systems, but some general features available only since Windows Vista.

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/memreduct.png)
