# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@451c6c485dfe456f26e0e68f260a008c4ff08003/icons/gitahead.png" height="48" width="48" /> ![GitAhead](https://img.shields.io/chocolatey/v/gitahead.svg?label=GitAhead&style=for-the-badge)](https://chocolatey.org/packages/gitahead)

GitAhead is a graphical Git client for Windows, Linux and macOS. It features a fast native interface designed to help you understand and manage your source code history.

GitAhead was designed by SciTools™, the makers of Understand™. It has all of the features you expect from a commercial quality client, now completely free and open source.

Git provideds a wealth of historical information that, too often, goes unexploited because it's cumbersome to explore with other tools. We believe that when you understand your history you'll make better choices today!
