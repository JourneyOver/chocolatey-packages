VERIFICATION

Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

The embedded software has been downloaded from the listed download
location on <https://www.axcrypt.net/download>
and can be verified by doing the following:

1. Download the following:

  url: <https://account.axcrypt.net/download/AxCrypt-2-Setup.exe>

2. You can obtain the checksum using one of the following methods:
  - Use powershell function 'Get-FileHash'
  - Use Chocolatey utility 'checksum.exe'
  - check <https://www.axcrypt.net/cryptographic-hashes-files>

  checksum type: sha256
  checksum: B30273015669229D7E924A7D2FC7F350C667F1441605CA7BD5DE22FF5C0ACCA4

Using AU:

  Get-RemoteChecksum

The file 'LICENSE.txt' is obtained from:
  <https://www.axcrypt.net/documentation/license> at "Software License"
