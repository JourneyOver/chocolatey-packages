# [<img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/metropolislauncher.png" height="48" width="48" /> ![Metropolis Launcher](https://img.shields.io/chocolatey/v/metropolislauncher.svg?label=Metropolis%20Launcher&style=for-the-badge)](https://chocolatey.org/packages/metropolislauncher)

Metropolis Launcher has been created to be a great old-school launcher, emulation front-end and an extensive offline database of video game metadata thanks to [MobyGames](https://www.mobygames.com/) and their strong user base.

## Features

- [Features](https://metropolis-launcher.net/#features)

## Community

- [Twitter](https://twitter.com/theMK2k)
- [Reddit](https://www.reddit.com/r/metropolislauncher)

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/metropolislauncher.png)
