# <img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/any2ico.png" width="48" height="48"/> [Quick Any2Ico](https://chocolatey.org/packages/any2ico.portable)

There are a number of icons that are included with programs and images, that users might need to work with. Quick Any2Ico is an application that allows users to extract icons from various different file formats, then convert them into either PNG or ICO images in several different resolution options.

## Features

- A single window interface that displays all the programs options and settings within a single window
- A lack of advanced options and customization within the application that makes it more basic
- A reasonable application for users only interested in the basic icon extraction and nothing else

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/any2ico.png)
