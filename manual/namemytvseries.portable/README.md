# <img src="https://cdn.jsdelivr.net/gh/JourneyOver/chocolatey-packages@2abe074087be5f4c921b6ab1ad0bc6ccd959bbfa/icons/namemytvseries.png" width="48" height="48"/> [Name My TV Series](https://chocolatey.org/packages/namemytvseries.portable)

Name My TV Series is free and available for Windows, Linux and Mac OS X. It retrieves data for the proper episode names from TheTVDB and/or EPGuide to assist you in renaming your downloaded TV Show episodes individually or in bulk. It supports drag and drop, all common video formats, is flexible in the renaming patterns, and renames related files (.nfo, .srt, etc).

![screenshot](https://raw.githubusercontent.com/JourneyOver/chocolatey-packages/master/readme_imgs/namemytvseries.png)
